import React, { useState } from 'react';
import './App.css';
import Search from './components/Search';
import { Selection } from './components/Selection';
import { data } from './data';
function App() {
  const [app, setApp] = useState(true);
  return (
    <div className="app">
      <header className="">
        <h1>Please choose component</h1>
        <button onClick={() => setApp(true)}>Search component</button>
        <button onClick={() => setApp(false)}>Selection component</button>
        <div className="app-component">
          {app ? <Search /> : <Selection data={data}/>}
        </div>
      </header>
    </div>
  );
}

export default App;
