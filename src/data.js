export const data = [
  {
    name: 'US',
    id: 1,
    type: 'country',
    isChecked: true,
    items: [
      {
        name: 'California',
        id: 11,
        type: 'state',
        items: [
          {
            name: 'Los Angeles',
            id: 111,
            type: 'city',
          },
          {
            name: 'Los Angeles',
            id: 112,
            type: 'city',
          },
          {
            name: 'Los Angeles',
            id: 113,
            type: 'city',
          },
        ]
      },
      {
        name: 'California',
        id: 12,
        type: 'state',
        items: [
          {
            name: 'Los Angeles',
            id: 121,
            type: 'city',
          },
          {
            name: 'Los Angeles',
            id: 122,
            type: 'city',
          },
          {
            name: 'Los Angeles',
            id: 123,
            type: 'city',
          },
        ]
      },
    ]
  },
  {
    name: 'Viet Nam',
    id: 2,
    type: 'country',
    items: [
      {
        name: 'Dong Nai',
        id: 21,
        type: 'province',
        items: [
          {
            name: 'Bien Hoa',
            id: 211,
            type: 'city',
            items: [
              {
                name: 'Hoa An',
                id: 2111,
                type: 'ward'
              },
            ]
          },
          {
            name: 'Bien Hoa',
            id: 212,
            type: 'city',
          },
          {
            name: 'Bien Hoa',
            id: 213,
            type: 'city',
          },
          {
            name: 'Bien Hoa',
            id: 214,
            type: 'city',
          },
        ]
      },
    ]
  },
]