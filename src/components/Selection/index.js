import React, { useState } from 'react';
import classNames from 'classnames';
import './index.scss';

export const Selection = ({ data }) => {
  const [selectedIds, setSelectedIds] = useState([]);

  const toggleSelectionId = (id, shouldAddToList) => {
    if (shouldAddToList) {
      setSelectedIds([...selectedIds, id]);
    } else {
      setSelectedIds(selectedIds.filter(i => i !== id));
    }
  };

  const toggleSelectionIds = (ids, shouldAddToList) => {
    if (shouldAddToList) {
      setSelectedIds([...selectedIds, ...ids]);
    } else {
      setSelectedIds(selectedIds.filter(i => ids.indexOf(i) === -1));
    }
  };

  const checkHasChildren = ({ items }) => items && items.length;

  const getRelatedIds = item => {
    const { id, items } = item;
    const ids = [id];
    items.forEach(i => {
      if (checkHasChildren(i)) {
        ids.push(...getRelatedIds(i));
      }
      ids.push(i.id);
    });

    return ids;
  };

  const handleToggle = (e, item, isChecked) => {
    e.stopPropagation();
    handleSelect(item, isChecked);
  };

  const handleSelect = (item, isChecked) => {
    const { id } = item;
    const hasChildren = checkHasChildren(item);
    const shouldAddToList = !isChecked;
    if (!hasChildren) {
      toggleSelectionId(id, shouldAddToList);
    } else {
      const ids = getRelatedIds(item);
      toggleSelectionIds(ids, shouldAddToList);
    }
  };

  const checkPartial = item =>
    checkHasChildren(item) && item.items.some(i => checkChecked(i));

  const checkChecked = item => {
    if (checkHasChildren(item)) {
      return item.items.every(i => checkChecked(i));
    }
    return selectedIds.indexOf(item.id) !== -1;
  };

  const renderData = (item, parentChecked) => {
    const { id, name, items } = item;
    const isChecked = parentChecked || checkChecked(item);
    const isPartial = !isChecked && checkPartial(item);

    return (
      <div key={id} className="item-wrapper">
        <h6
          onClick={e => handleToggle(e, item, isChecked)}
          className={classNames('item-title', {
            'is-checked': isChecked,
            'is-partial': isPartial,
          })}
        >
          {name}
        </h6>
        {items && items.length && items.map(i => renderData(i, isChecked))}
      </div>
    );
  };

  return (
    <>
    <h1><span className="is-partial">Yellow</span> is partial, <span className="is-checked">Green</span> is selected</h1>
    <div className="selection-wrapper">
      {data.length ? data.map(i => renderData(i)) : null}
    </div>
    </>
  )
};
