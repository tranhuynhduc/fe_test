import React from 'react';

const highlightSearchValue = (title, searchValue) => {

  const reg = new RegExp(searchValue, 'ig')
  const result = title.replace(reg, (value) => {
    return `<span class="highlight-text">${value}</span>`
  });
  return result;
}
const SearchResult = ({items, searchValue}) => {
  if (!items || !items.length) {
    return null;
  }

  return (
    <ul className="search-result">
      {
        items.map(({objectID, title}) => (
          title ? (
            <li
              key={objectID}
              className="result-item"
              dangerouslySetInnerHTML={{ __html: highlightSearchValue(title, searchValue)}}
            />
           ) : null
        ))
      }
    </ul>
  )
}

export default SearchResult;
