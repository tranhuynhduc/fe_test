import React, { useState } from 'react';
import { SEARCH_API } from '../../utils/constants';
import { request } from '../../utils/request';
import './index.scss';
import SearchResult from '../SearchResult';

const Search = () => {
  const [searchResult, setSearchResult] = useState([]);
  const [searchTimer, setSearchTimer] = useState(null);
  const [searchValue, setSearchValue] = useState('');

  const handleInputChnge = async (e) => {
    const value = e.target.value;

    clearTimeout(searchTimer);
    const timeout = setTimeout(() => {
      if (!value) {
        setSearchResult([]);
        return;
      }

      getSearchData(value);
    }, 250);
    setSearchValue(value);
    setSearchTimer(timeout);
  };

  const getSearchData = async searchValue => {
    try {
      const url = `${SEARCH_API}?query=${searchValue}`
      const data = await request(url);
      setSearchResult(data.hits);
    } catch (err) {
      console.error(err);
    }
  };

  const handleClear = () => {
    setSearchResult([]);
    setSearchValue('');
  }

  return (
    <div className="search-container">
      <div className="search-wrapper">
        <div className="search-input-wrapper">
          <input
            className="search-input"
            type="text"
            placeholder="Input your query"
            value={searchValue} onChange={handleInputChnge}/>
          <button className="clear-btn" onClick={handleClear}>Clear</button>
        </div>
        <SearchResult items={searchResult} searchValue={searchValue} />
      </div>
    </div>
  )
}

export default Search;

