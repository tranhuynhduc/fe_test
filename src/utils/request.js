const getResponseBody = response => {
  const contentType = response.headers.get('content-type');
  return contentType && contentType.indexOf('json') >= 0
    ? response.text().then(tryParseJSON)
    : response.text();
};

const tryParseJSON = json => {
  if (!json) return null;

  try {
    return JSON.parse(json);
  } catch (e) {
    throw new Error(`Failed to parse unexpected JSON response: ${json}`);
  }
};

export const request = async (url, options) => {
  const response = await fetch(url, options);

  const body = await getResponseBody(response) ;
  console.log('body', body);
  return body;
}