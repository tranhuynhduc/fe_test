## How to run the app

```bash
yarn
yarn start
```
or
```bash
npm install
npm run start
```

## How to use the app
There are two buttons to switch between question 1 and question 2